module git.autistici.org/ai3/tools/yarascan

go 1.14

require (
	git.autistici.org/ai3/go-common v0.0.0-20210110180225-a05c683cfe23
	github.com/elazarl/go-bindata-assetfs v1.0.1
	github.com/golang-migrate/migrate/v4 v4.14.1
	github.com/google/subcommands v1.2.0
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hillu/go-yara/v4 v4.0.6
	gopkg.in/yaml.v2 v2.4.0
)
