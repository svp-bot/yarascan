package yarascan

import (
	"os"
	"time"
)

var hostname string

func init() {
	hostname, _ = os.Hostname()
}

// Detection entry.
type Detection struct {
	Host      string    `json:"host"`
	Path      string    `json:"path"`
	Signature string    `json:"signature"`
	Timestamp time.Time `json:"timestamp"`
}

// NewDetection builds a new Detection object for the current host.
func NewDetection(path, signature string) *Detection {
	return &Detection{
		Host:      hostname,
		Path:      path,
		Signature: signature,
		Timestamp: time.Now().UTC(),
	}
}

// SubmissionRequest is the request type for /api/submission.
type SubmissionRequest struct {
	Detections []*Detection `json:"detections"`
}

// PathDetection summarizes the detections by path, aggregating all
// non-resolved detections over time.
type PathDetection struct {
	Host       string    `json:"host"`
	Path       string    `json:"path"`
	Site       string    `json:"site"`
	Signatures []string  `json:"signatures"`
	FirstStamp time.Time `json:"first_stamp"`
	LastStamp  time.Time `json:"last_stamp"`
}

// FindDetectionsBySiteRequest is the request type for /api/search/by_site.
type FindDetectionsBySiteRequest struct {
	Site string `json:"site"`
}

// FindDetectionsBySiteResponse is the result type for FindDetectionsBySiteRequest.
type FindDetectionsBySiteResponse struct {
	Detections []*PathDetection `json:"detections"`
}
