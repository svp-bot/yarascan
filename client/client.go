package client

import (
	"context"

	"git.autistici.org/ai3/go-common/clientutil"
	"git.autistici.org/ai3/tools/yarascan"
)

type Client struct {
	be clientutil.Backend
}

func New(config *clientutil.BackendConfig) (*Client, error) {
	be, err := clientutil.NewBackend(config)
	if err != nil {
		return nil, err
	}
	return &Client{be}, nil
}

func (c *Client) Submit(ctx context.Context, dd []*yarascan.Detection) error {
	req := yarascan.SubmissionRequest{
		Detections: dd,
	}
	return c.be.Call(ctx, "", "/api/submission", &req, nil)
}

func (c *Client) FindDetectionsBySite(ctx context.Context, site string) ([]*yarascan.PathDetection, error) {
	var resp yarascan.FindDetectionsBySiteResponse
	if err := c.be.Call(ctx, "", "/api/search/by_site", &yarascan.FindDetectionsBySiteRequest{
		Site: site,
	}, &resp); err != nil {
		return nil, err
	}
	return resp.Detections, nil
}
