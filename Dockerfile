FROM debian:bullseye AS build
RUN apt-get -q update && env DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends build-essential libyara-dev pkg-config golang-go

ADD . /src
WORKDIR /src
RUN go test -v ./... && \
    go build -o /yarascan ./cmd/yarascan && \
    strip /yarascan

FROM debian:bullseye
RUN apt-get -q update && env DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends libyara4
COPY --from=build /yarascan /usr/bin/yarascan

ENTRYPOINT ["/usr/bin/yarascan"]
