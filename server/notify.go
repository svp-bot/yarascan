package server

import (
	"context"
	"fmt"
	"log"
	"strings"
	"time"

	"git.autistici.org/ai3/tools/yarascan"
)

var notifyInterval = 600 * time.Second

func (s *Server) notify(ctx context.Context, site string, dd []*yarascan.PathDetection) error {
	log.Printf("notify: %s (%d)", site, len(dd))

	if s.mailer == nil {
		return nil
	}

	var rcpts []string
	if s.notifyAddr != "" {
		rcpts = append(rcpts, s.notifyAddr)
	}
	if s.siteOwners != nil {
		if owner, ok := s.siteOwners[site]; ok {
			rcpts = append(rcpts, owner)
		}
	}

	// Send separate emails to each recipient (if any).
	for _, rcpt := range rcpts {
		err := s.mailer.SendPlainTextMessage(
			"malware_found",
			"en",
			fmt.Sprintf("Malware found in website %s", site),
			rcpt,
			map[string]interface{}{
				"Site":       site,
				"Detections": dd,
			},
		)
		if err != nil {
			// If the error is about an unknown recipient,
			// or otherwise it's a permanent error, log
			// but do not retry.
			if errStr := err.Error(); strings.HasPrefix(errStr, "5") {
				log.Printf("permanent error notifying %s: %s", rcpt, errStr)
			} else {
				return err
			}
		}
	}

	return nil
}

func (s *Server) notifyAllPending(ctx context.Context) (int, error) {
	tx, err := s.db.Begin()
	if err != nil {
		return 0, err
	}

	var n int
	setNotified := s.stmts.Get(tx, "set_site_notified")

	// Iterate over the pending detections, grouped by site.
	bySite := make(map[string][]*yarascan.PathDetection)
	for _, d := range s.findUnnotifiedDetections(tx) {
		bySite[d.Site] = append(bySite[d.Site], d)
	}
	for site, dd := range bySite {
		if err = s.notify(ctx, site, dd); err != nil {
			break
		}

		// Mark individually each PathDetection as notified.
		for _, d := range dd {
			if _, err = setNotified.Exec(d.Site, d.Path, d.LastStamp); err != nil {
				break
			}
		}

		n++
	}

	// The commit error takes precedence.
	if cerr := tx.Commit(); cerr != nil {
		err = cerr
	}
	return n, err
}

func (s *Server) notifyLoop(stop <-chan bool) {
	t := time.NewTicker(notifyInterval)
	for {
		select {
		case <-t.C:
			n, err := s.notifyAllPending(context.Background())
			if err != nil {
				log.Printf("error: could not send all notifications: %v", err)
			} else {
				log.Printf("sent %d notifications", n)
			}
		case <-stop:
			return
		}
	}
}
