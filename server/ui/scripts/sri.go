package main

import (
	"crypto/sha512"
	"encoding/base64"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
)

var (
	outputPath  = flag.String("output", "", "output `file` name")
	packageName = flag.String("package", "", "`name` of the package for generated code")
	stripPrefix = flag.String("strip", "", "prefix to strip from `path`s to generate URLs")
)

func computeChecksum(path string) (string, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return "", err
	}
	sha := sha512.Sum384(data)
	return "sha384-" + base64.StdEncoding.EncodeToString(sha[:]), nil
}

func urlForPath(path string) string {
	path = strings.TrimPrefix(path, *stripPrefix)
	if !strings.HasPrefix(path, "/") {
		path = "/" + path
	}
	return path
}

func mkSRIMap(m map[string]string, dir string) error {
	return filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		// Only match files with the desired extensions.
		if info.IsDir() || !match(info.Name()) {
			return nil
		}

		if cksum, err := computeChecksum(path); err == nil {
			m[urlForPath(path)] = cksum
		}
		return nil
	})
}

func match(name string) bool {
	switch filepath.Ext(name) {
	case ".js", ".json", ".css":
		return true
	default:
		return false
	}
}

// nolint: errcheck
func codegen(w io.Writer, m map[string]string) {
	fmt.Fprintf(w, "package %s\n", *packageName)
	io.WriteString(w, `
import (
	"fmt"
	"html/template"
)

var sriMap = map[string]string{
`)
	for k, v := range m {
		fmt.Fprintf(w, "\t%q: %q,\n", k, v)
	}
	io.WriteString(w, `}

// SRIScript returns a <script> tag with resource integrity attributes.
func SRIScript(uri string) template.HTML {
	s := fmt.Sprintf("<script src=\"%s\"", uri)
	if sri, ok := sriMap[uri]; ok {
		s += fmt.Sprintf(" crossorigin=\"\" integrity=\"%s\"", sri)
	}
	s += "></script>"
	return template.HTML(s) // nolint: gosec
}

// SRIStylesheet returns a <link> tag with resource integrity attributes.
func SRIStylesheet(uri string) template.HTML {
	s := fmt.Sprintf("<link rel=\"stylesheet\" type=\"text/css\" href=\"%s\"", uri)
	if sri, ok := sriMap[uri]; ok {
		s += fmt.Sprintf(" integrity=\"%s\"", sri)
	}
	s += ">"
	return template.HTML(s) // nolint: gosec
}

`)
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	m := make(map[string]string)
	for _, path := range flag.Args() {
		if err := mkSRIMap(m, path); err != nil {
			log.Println(err)
		}
	}

	var w io.Writer = os.Stdout
	if *outputPath != "" {
		var err error
		w, err = os.Create(*outputPath)
		if err != nil {
			log.Fatal(err)
		}
	}
	codegen(w, m)
}
