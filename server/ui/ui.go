package ui

//go:generate go-bindata --nocompress --pkg ui static/... templates/...
//go:generate go run scripts/sri.go --package ui --output sri_auto.go static

import (
	"bytes"
	"database/sql"
	"fmt"
	"html/template"
	"io"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"git.autistici.org/ai3/tools/yarascan/server"
	assetfs "github.com/elazarl/go-bindata-assetfs"
)

type UI struct {
	db  *sql.DB
	tpl *template.Template
}

func New(db *sql.DB) *UI {
	return &UI{
		db:  db,
		tpl: mustParseEmbeddedTemplates(),
	}
}

type queryBuilder struct {
	lead         string
	clauses      []string
	finalClauses []string
	args         []interface{}
}

func newQuery(baseQuery string) *queryBuilder {
	return &queryBuilder{
		lead: baseQuery,
	}
}

func (q *queryBuilder) OrderBy(col, order string) *queryBuilder {
	q.finalClauses = append(q.finalClauses, fmt.Sprintf("ORDER BY %s %s", col, order))
	return q
}

func (q *queryBuilder) Limit(n int) *queryBuilder {
	q.finalClauses = append(q.finalClauses, fmt.Sprintf("LIMIT %d", n))
	return q
}

func (q *queryBuilder) Offset(n int) *queryBuilder {
	q.finalClauses = append(q.finalClauses, fmt.Sprintf("OFFSET %d", n))
	return q
}

func (q *queryBuilder) Where(clause string, args ...interface{}) *queryBuilder {
	q.clauses = append(q.clauses, clause)
	q.args = append(q.args, args...)
	return q
}

// nolint: errcheck
func (q *queryBuilder) query() (string, []interface{}) {
	var buf bytes.Buffer
	io.WriteString(&buf, q.lead)
	if len(q.clauses) > 0 {
		io.WriteString(&buf, " WHERE (")
		for i, c := range q.clauses {
			if i > 0 {
				io.WriteString(&buf, ") AND (")
			}
			io.WriteString(&buf, c)
		}
		io.WriteString(&buf, ")")
	}
	if len(q.finalClauses) > 0 {
		io.WriteString(&buf, " ")
		io.WriteString(&buf, strings.Join(q.finalClauses, " "))
	}
	return buf.String(), q.args
}

type query interface {
	query() (string, []interface{})
}

func (ui *UI) getDetections(q query) ([]*server.RawDetection, error) {
	tx, err := ui.db.Begin()
	if err != nil {
		return nil, err
	}
	defer tx.Rollback() // nolint

	stmt, args := q.query()
	rows, err := tx.Query(stmt, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var out []*server.RawDetection
	for rows.Next() {
		var d server.RawDetection
		if err := rows.Scan(&d.ID, &d.Site, &d.Host, &d.Path, &d.Signature, &d.Timestamp, &d.Resolved, &d.Notified); err != nil {
			log.Printf("Scan() error: %v", err)
			continue
		}
		out = append(out, &d)
	}

	return out, rows.Err()
}

type countResult struct {
	Value string
	Count int
}

func (ui *UI) getCounts(fieldName string, n int) ([]countResult, error) {
	tx, err := ui.db.Begin()
	if err != nil {
		return nil, err
	}
	defer tx.Rollback() // nolint

	rows, err := tx.Query(fmt.Sprintf(
		"SELECT %s, COUNT(*) AS c FROM detections GROUP BY %s ORDER BY c DESC LIMIT %d",
		fieldName,
		fieldName,
		n,
	))
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	out := make([]countResult, 0, n)
	for rows.Next() {
		var value string
		var count int
		if err := rows.Scan(&value, &count); err != nil {
			log.Printf("Scan() error: %v", err)
			continue
		}
		out = append(out, countResult{Value: value, Count: count})
	}

	return out, rows.Err()
}

type searchParams struct {
	url *url.URL

	Host      string
	Site      string
	Path      string
	Signature string
	AgeDays   int
	Limit     int
	Offset    int
}

func intFormValue(req *http.Request, arg string) int {
	if s := req.FormValue(arg); s != "" {
		if n, err := strconv.Atoi(s); err == nil {
			return n
		}
	}
	return 0
}

func parseSearchParams(req *http.Request) *searchParams {
	var p searchParams

	p.url = req.URL
	p.Host = req.FormValue("host")
	p.Site = req.FormValue("site")
	p.Path = req.FormValue("path")
	p.Signature = req.FormValue("sig")
	p.AgeDays = intFormValue(req, "age")
	p.Limit = intFormValue(req, "n")
	if p.Limit == 0 {
		p.Limit = 20
	}
	p.Offset = intFormValue(req, "o")

	return &p
}

func (p *searchParams) query() query {
	q := newQuery("SELECT id, site, host, path, signature, timestamp, resolved, notified FROM detections")

	if p.Host != "" {
		q = q.Where("host = ?", p.Host)
	}
	if p.Site != "" {
		q = q.Where("site = ?", p.Site)
	}
	if p.Path != "" {
		q = q.Where("path LIKE ?", strings.Replace(p.Path, "*", "%", -1))
	}
	if p.Signature != "" {
		q = q.Where("signature LIKE ?", strings.Replace(p.Signature, "*", "%", -1))
	}
	if p.AgeDays != 0 {
		cutoff := time.Now().Add(time.Hour * time.Duration(-24*p.AgeDays))
		q = q.Where("timestamp > ?", cutoff)
	}

	q = q.OrderBy("timestamp", "desc")
	q = q.Limit(p.Limit)
	if p.Offset != 0 {
		q = q.Offset(p.Offset)
	}

	return q
}

func (p *searchParams) WithSite(site string) *searchParams {
	p2 := *p
	p2.Site = site
	p2.Offset = 0
	return &p2
}

func (p *searchParams) WithHost(host string) *searchParams {
	p2 := *p
	p2.Host = host
	p2.Offset = 0
	return &p2
}

func (p *searchParams) Prev() *searchParams {
	if p.Offset == 0 {
		return nil
	}
	prev := new(searchParams)
	*prev = *p
	prev.Offset = p.Offset - p.Limit
	if prev.Offset < 0 {
		prev.Offset = 0
	}
	return prev
}

func (p *searchParams) Next() *searchParams {
	next := new(searchParams)
	*next = *p
	next.Offset = p.Offset + p.Limit
	return next
}

func (p *searchParams) URL() string {
	v := make(url.Values)
	if p.Host != "" {
		v.Set("host", p.Host)
	}
	if p.Site != "" {
		v.Set("site", p.Site)
	}
	if p.Path != "" {
		v.Set("path", p.Path)
	}
	if p.Signature != "" {
		v.Set("sig", p.Signature)
	}
	if p.AgeDays != 0 {
		v.Set("age", strconv.Itoa(p.AgeDays))
	}
	if p.Limit != 0 {
		v.Set("n", strconv.Itoa(p.Limit))
	}
	if p.Offset != 0 {
		v.Set("o", strconv.Itoa(p.Offset))
	}

	u := *p.url
	u.RawQuery = v.Encode()
	return u.String()
}

func (p *searchParams) String() string {
	return p.URL()
}

func (ui *UI) renderTemplate(w http.ResponseWriter, tplName string, values map[string]interface{}) {
	var buf bytes.Buffer
	if err := ui.tpl.Lookup(tplName).Execute(&buf, values); err != nil {
		log.Printf("template error: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Header().Set("Cache-Control", "no-cache, no-store")
	io.Copy(w, &buf) // nolint: errcheck
}

func (ui *UI) handleSearch(w http.ResponseWriter, req *http.Request) {
	if req.URL.Path != "/" {
		http.NotFound(w, req)
		return
	}

	params := parseSearchParams(req)
	results, err := ui.getDetections(params.query())
	if err != nil {
		log.Printf("sql error: %v", err)
	}

	ui.renderTemplate(w, "search.html", map[string]interface{}{
		"Results": results,
		"Params":  params,
		"Error":   err,
	})
}

func (ui *UI) handleOverview(fieldName string) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		counts, err := ui.getCounts(fieldName, 20)
		ui.renderTemplate(w, "overview.html", map[string]interface{}{
			"FieldName": fieldName,
			"Counts":    counts,
			"Error":     err,
		})
	})
}

func (ui *UI) Handler() http.Handler {
	mux := http.NewServeMux()

	mux.Handle("/static/", http.StripPrefix("/static/", http.FileServer(&assetfs.AssetFS{
		Asset:     Asset,
		AssetDir:  AssetDir,
		AssetInfo: AssetInfo,
		Prefix:    "static",
	})))
	mux.HandleFunc("/", ui.handleSearch)
	mux.HandleFunc("/sites", ui.handleOverview("site"))
	mux.HandleFunc("/hosts", ui.handleOverview("host"))
	return mux
}

// Parse the templates that are embedded with the binary (in bindata.go).
func mustParseEmbeddedTemplates() *template.Template {
	root := template.New("").Funcs(template.FuncMap{
		// These are loaded from the auto-generated sri_auto.go file.
		"sri_script":     SRIScript,
		"sri_stylesheet": SRIStylesheet,
	})
	files, err := AssetDir("templates")
	if err != nil {
		log.Fatalf("no asset dir for templates: %v", err)
	}
	for _, f := range files {
		b, err := Asset("templates/" + f)
		if err != nil {
			log.Fatalf("could not read embedded template %s: %v", f, err)
		}
		if _, err := root.New(f).Parse(string(b)); err != nil {
			log.Fatalf("error parsing template %s: %v", f, err)
		}
	}
	return root
}
