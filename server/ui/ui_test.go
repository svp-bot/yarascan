package ui

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"strings"
	"testing"
	"time"

	"git.autistici.org/ai3/tools/yarascan/server/db"
)

func TestUI_TemplateBuildsSuccessfully(t *testing.T) {
	dbFile := "test.db"
	defer os.Remove(dbFile)

	database, err := db.Open(dbFile)
	if err != nil {
		t.Fatal(err)
	}
	defer database.Close()

	New(database)
}

func TestUI_Search(t *testing.T) {
	dbFile := "test.db"
	defer os.Remove(dbFile)

	database, err := db.Open(dbFile)
	if err != nil {
		t.Fatal(err)
	}
	defer database.Close()

	// Insert a random record.
	if _, err := database.Exec(`
INSERT INTO detections (id, site, host, path, signature, timestamp) VALUES (?, ?, ?, ?, ?, ?)
`, 1, "site1", "host1", "/path/to/site/malware.php", "malware", time.Now()); err != nil {
		t.Fatal(err)
	}

	ui := New(database)
	srv := httptest.NewServer(ui.Handler())
	defer srv.Close()
	resp, err := http.Get(srv.URL)
	if err != nil {
		t.Fatalf("Get(/): %v", err)
	}
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)

	if resp.StatusCode != 200 {
		t.Fatalf("Get(/) returned %s", resp.Status)
	}
	sbody := string(body)
	if !strings.Contains(sbody, "site1") {
		t.Fatalf("body does not contain detections:\n%s", sbody)
	}
}

func TestUI_SearchParams_Navigation(t *testing.T) {
	p := parseSearchParams(&http.Request{
		URL: new(url.URL),
	})
	if p.Limit != 20 {
		t.Fatalf("default Limit is %d, expected 20", p.Limit)
	}
	if p.Offset != 0 {
		t.Fatalf("default offset is %d, expected 0", p.Offset)
	}

	next := p.Next()
	if next == nil {
		t.Fatal("Next() is nil")
	}
	if next.Limit != 20 {
		t.Fatalf("next.Limit is %d, expected 20", next.Limit)
	}
	if next.Offset != 20 {
		t.Fatalf("next.Offset is %d, expected 20", next.Offset)
	}

	prev := p.Prev()
	if prev != nil {
		t.Fatal("Prev() is not nil")
	}
	prev = next.Prev()
	if prev.Limit != 20 {
		t.Fatalf("next.Prev().Limit is %d, expected 20", next.Limit)
	}
	if prev.Offset != 0 {
		t.Fatalf("next.Prev().Offset is %d, expected 0", next.Offset)
	}
}
