// Package server implements the collection of detection events into a
// centralized database with an HTTP API.
//
// The difficulty in supporting both full and incremental scan modes
// is that we do not have a reliable signal for the disappearance of a
// detection. To maintain flexibility, the data model stores every raw
// detection entry and synthesizes path-level information from it, and
// we support an external method to explicitly acknowledge (resolve)
// entries.
//
package server

import (
	"database/sql"
	"log"
	"net/http"
	"strings"
	"time"

	"git.autistici.org/ai3/go-common/mail"
	"git.autistici.org/ai3/go-common/serverutil"
	"git.autistici.org/ai3/tools/yarascan"
	"git.autistici.org/ai3/tools/yarascan/server/db"
)

// RawDetection is a detection event as stored in the database: a
// yarascan.Detection with additional site and state information.
type RawDetection struct {
	yarascan.Detection

	ID       int64
	Site     string
	Notified bool
	Resolved bool
}

var sqliteTimeFormat = "2006-01-02 15:04:05.999999999Z07:00"

var sqlStatements = map[string]string{

	"insert_detection": `
INSERT INTO detections (site, host, path, signature, timestamp) VALUES (?, ?, ?, ?, ?)
`,
	"find_raw_detections_by_site": `
SELECT
  id, site, host, path, signature, timestamp, resolved, notified
FROM
  detections
WHERE
  site = ?
`,
	"find_detections_by_site": `
SELECT
  site, host, path, GROUP_CONCAT(signature) AS signatures, MIN(timestamp) AS first_stamp, MAX(timestamp) AS last_stamp
FROM
  detections
WHERE
  site = ? AND resolved = 0
GROUP BY
  host, path
`,
	"find_unnotified_detections": `
SELECT
  site, host, path, GROUP_CONCAT(signature) AS signatures, MIN(timestamp) AS first_stamp, MAX(timestamp) AS last_stamp
FROM
  detections
WHERE
  resolved = 0 AND notified = 0
GROUP BY
  site, host, path
`,
	"latest_unacknowledged_detections": `
SELECT
  id, site, host, path, signature, timestamp, resolved, notified
FROM
  detections
WHERE
  resolved = 0
ORDER BY timestamp DESC LIMIT ?
`,
	"resolve_by_site": `
UPDATE detections SET resolved = 1 WHERE site = ?
`,
	"set_site_notified": `
UPDATE detections SET notified = 1 WHERE notified = 0 AND site = ? AND path = ? AND timestamp <= ?
`,
}

// Config for a Server instance.
type Config struct {
	HomedirMap   string       `yaml:"homedir_map_file"`
	SiteOwnerMap string       `yaml:"site_owner_map_file"`
	NotifyAddr   string       `yaml:"notify_addr"`
	Mail         *mail.Config `yaml:"mail"`
}

// Server for the yarascan HTTP API.
type Server struct {
	homedirs   homedirMap
	siteOwners siteOwnerMap
	notifyAddr string
	mailer     *mail.Mailer

	db     *sql.DB
	stmts  db.StatementMap
	doneCh chan bool
	stopCh chan bool
}

// New returns a new Server.
func New(database *sql.DB, config *Config) (*Server, error) {
	var err error
	var homedirs homedirMap
	if config.HomedirMap != "" {
		if homedirs, err = loadHomedirMap(config.HomedirMap); err != nil {
			return nil, err
		}
	}

	var siteOwners siteOwnerMap
	if config.SiteOwnerMap != "" {
		if siteOwners, err = loadSiteOwnerMap(config.SiteOwnerMap); err != nil {
			return nil, err
		}
	}

	var mailer *mail.Mailer
	if config.Mail != nil {
		mailer, err = mail.New(config.Mail)
		if err != nil {
			return nil, err
		}
	}

	stmts, err := db.NewStatementMap(database, sqlStatements)
	if err != nil {
		return nil, err
	}

	s := &Server{
		homedirs:   homedirs,
		siteOwners: siteOwners,
		notifyAddr: config.NotifyAddr,
		mailer:     mailer,
		db:         database,
		stmts:      stmts,
		stopCh:     make(chan bool),
		doneCh:     make(chan bool),
	}

	// Start the notification thread in the background.
	go func() {
		s.notifyLoop(s.stopCh)
		close(s.doneCh)
	}()

	return s, nil
}

// Close the Server and all associated resources.
func (s *Server) Close() {
	close(s.stopCh)
	s.stmts.Close()
	<-s.doneCh
}

// Post-process the incoming Detection objects before they are saved
// to the database. Use this function to add whatever metadata
// augmentation logic we desire.
func (s *Server) processDetection(d *yarascan.Detection) *RawDetection {
	rd := RawDetection{Detection: *d}
	if site, ok := s.homedirs.Find(d.Path); ok {
		rd.Site = site
	}
	return &rd
}

func (s *Server) insertDetections(tx *sql.Tx, dd []*yarascan.Detection) error {
	for _, d := range dd {
		rd := s.processDetection(d)
		_, err := s.stmts.Get(tx, "insert_detection").Exec(
			rd.Site,
			rd.Host,
			rd.Path,
			rd.Signature,
			rd.Timestamp,
		)
		if err != nil {
			tx.Rollback() // nolint
			return err
		}
	}

	return nil
}

// Generic method to retrieve RawDetections with a precompiled query.
func (s *Server) findRawDetections(tx *sql.Tx, queryName string, args ...interface{}) []*RawDetection {
	rows, err := s.stmts.Get(tx, queryName).Query(args...)
	if err != nil {
		return nil
	}
	defer rows.Close()

	var out []*RawDetection
	for rows.Next() {
		var rd RawDetection
		var resolved, notified sql.NullBool
		if err := rows.Scan(&rd.ID, &rd.Site, &rd.Host, &rd.Path, &rd.Signature, &rd.Timestamp, &resolved, &notified); err != nil {
			continue
		}
		if resolved.Valid {
			rd.Resolved = resolved.Bool
		}
		if notified.Valid {
			rd.Notified = notified.Bool
		}
		out = append(out, &rd)
	}

	return out
}

func (s *Server) findDetections(tx *sql.Tx, queryName string, args ...interface{}) []*yarascan.PathDetection {
	rows, err := s.stmts.Get(tx, queryName).Query(args...)
	if err != nil {
		log.Printf("sql.Query() error: %v", err)
		return nil
	}
	defer rows.Close()

	var byPath []*yarascan.PathDetection
	for rows.Next() {
		var site, host, path, sigs string
		// For whatever reason sqlite returns these fields as
		// strings instead of datetime objects.
		var firstStamp, lastStamp string
		if err := rows.Scan(&site, &host, &path, &sigs, &firstStamp, &lastStamp); err != nil {
			log.Printf("Scan() error: %v", err)
			continue
		}
		pd := &yarascan.PathDetection{
			Site:       site,
			Host:       host,
			Path:       path,
			Signatures: strings.Split(sigs, ","),
		}
		if t, err := time.Parse(sqliteTimeFormat, firstStamp); err == nil {
			pd.FirstStamp = t
		}
		if t, err := time.Parse(sqliteTimeFormat, lastStamp); err == nil {
			pd.LastStamp = t
		}
		byPath = append(byPath, pd)
	}
	return byPath
}

func (s *Server) findUnnotifiedDetections(tx *sql.Tx) []*yarascan.PathDetection {
	return s.findDetections(tx, "find_unnotified_detections")
}

func (s *Server) execSimple(queryName string, args ...interface{}) error {
	tx, err := s.db.Begin()
	if err != nil {
		return err
	}

	_, err = s.stmts.Get(tx, queryName).Exec(args...)
	if err != nil {
		tx.Rollback() // nolint
		return err
	}

	return tx.Commit()
}

func (s *Server) resolveSite(site string) error {
	return s.execSimple("resolve_by_site", site)
}

// HTTP handlers

func (s *Server) handleSubmission(w http.ResponseWriter, r *http.Request) {
	var req yarascan.SubmissionRequest
	if !serverutil.DecodeJSONRequest(w, r, &req) {
		return
	}

	tx, err := s.db.Begin()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := s.insertDetections(tx, req.Detections); err != nil {
		log.Printf("submission error: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := tx.Commit(); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	serverutil.EncodeJSONResponse(w, nil)
}

func (s *Server) handleFindDetectionsBySite(w http.ResponseWriter, r *http.Request) {
	var req yarascan.FindDetectionsBySiteRequest
	if !serverutil.DecodeJSONRequest(w, r, &req) {
		return
	}

	tx, err := s.db.Begin()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer tx.Rollback() // nolint

	results := s.findDetections(tx, "find_detections_by_site", req.Site)

	serverutil.EncodeJSONResponse(w, &yarascan.FindDetectionsBySiteResponse{
		Detections: results,
	})
}

// Handler returns a http.Handler for the server API. All URL paths
// start with /api/.
func (s *Server) Handler() http.Handler {
	h := http.NewServeMux()
	h.HandleFunc("/api/search/by_site", s.handleFindDetectionsBySite)
	h.HandleFunc("/api/submission", s.handleSubmission)
	return h
}
