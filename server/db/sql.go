package db

import (
	"database/sql"
	"fmt"
	"log"

	migrate "github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/sqlite3"
	bindata "github.com/golang-migrate/migrate/v4/source/go_bindata"

	"git.autistici.org/ai3/tools/yarascan/migrations"
)

const dbDriver = "sqlite3"

// Open a SQLite database and run the database migrations.
func Open(dburi string) (*sql.DB, error) {
	db, err := sql.Open(dbDriver, dburi)
	if err != nil {
		return nil, err
	}

	// Serialize all queries onto a single connection.
	// Prevents sqlite database locking issues, at the
	// expense of performance.
	db.SetMaxOpenConns(1)

	if err = runDatabaseMigrations(db); err != nil {
		db.Close() // nolint
		return nil, err
	}

	return db, nil
}

type migrateLogger struct{}

func (l migrateLogger) Printf(format string, v ...interface{}) {
	log.Printf("db: "+format, v...)
}

func (l migrateLogger) Verbose() bool { return true }

func runDatabaseMigrations(db *sql.DB) error {
	si, err := bindata.WithInstance(bindata.Resource(
		migrations.AssetNames(),
		func(name string) ([]byte, error) {
			return migrations.Asset(name)
		}))
	if err != nil {
		return err
	}

	di, err := sqlite3.WithInstance(db, &sqlite3.Config{
		MigrationsTable: sqlite3.DefaultMigrationsTable,
		DatabaseName:    "usermetadb",
	})
	if err != nil {
		return err
	}

	m, err := migrate.NewWithInstance("bindata", si, dbDriver, di)
	if err != nil {
		return err
	}
	m.Log = &migrateLogger{}

	log.Printf("running database migrations")
	if err := m.Up(); err != nil && err != migrate.ErrNoChange {
		return err
	}
	return nil
}

// A StatementMap holds named compiled statements.
type StatementMap map[string]*sql.Stmt

// NewStatementMap compiles the given named statements and returns a
// new StatementMap.
func NewStatementMap(db *sql.DB, statements map[string]string) (StatementMap, error) {
	stmts := make(map[string]*sql.Stmt)
	for name, qstr := range statements {
		stmt, err := db.Prepare(qstr)
		if err != nil {
			return nil, fmt.Errorf("error compiling statement '%s': %v", name, err)
		}
		stmts[name] = stmt
	}
	return StatementMap(stmts), nil
}

// Close all resources associated with the StatementMap.
func (m StatementMap) Close() {
	for _, s := range m {
		s.Close() // nolint
	}
}

// Get a named compiled statement (or nil if not found), and associate
// it with the given transaction.
func (m StatementMap) Get(tx *sql.Tx, name string) *sql.Stmt {
	return tx.Stmt(m[name])
}
