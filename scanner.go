// Most code lifted from https://github.com/botherder/kraken.
//
package yarascan

import (
	"log"
	"os"
	"path"
	"path/filepath"
	"time"

	"github.com/hillu/go-yara/v4"
)

// Scanner can apply YARA rules to files.
type Scanner struct {
	rules *yara.Rules
}

// NewScanner loads YARA rules from rulesPath and returns a new Scanner.
func NewScanner(rulesPath string) (*Scanner, error) {
	s := new(Scanner)
	if err := s.compile(rulesPath); err != nil {
		return nil, err
	}
	return s, nil
}

func (s *Scanner) compile(rulesPath string) error {
	compiler, err := yara.NewCompiler()
	if err != nil {
		return err
	}

	rulesStat, err := os.Stat(rulesPath)
	if err != nil {
		return err
	}
	switch mode := rulesStat.Mode(); {
	case mode.IsDir():
		err = filepath.Walk(rulesPath, func(filePath string, fileInfo os.FileInfo, err error) error {
			fileName := fileInfo.Name()

			// Check if the file has extension .yar or .yara.
			if (path.Ext(fileName) == ".yar") || (path.Ext(fileName) == ".yara") {
				log.Printf("Adding rule %s", filePath)

				// Open the rule file and add it to the Yara compiler.
				rulesFile, _ := os.Open(filePath)
				defer rulesFile.Close()
				err = compiler.AddFile(rulesFile, "")
				if err != nil {
					panic(err)
				}
			}
			return nil
		})
	case mode.IsRegular():
		log.Printf("Compiling Yara rule %s", rulesPath)
		rulesFile, _ := os.Open(rulesPath)
		defer rulesFile.Close()
		err = compiler.AddFile(rulesFile, "")
		if err != nil {
			panic(err)
		}
	}

	// Collect and compile Yara rules.
	s.rules, err = compiler.GetRules()
	if err != nil {
		return err
	}

	return nil
}

type matchList []*yara.Rule

func (l *matchList) RuleMatching(ctx *yara.ScanContext, rule *yara.Rule) (bool, error) {
	*l = append(*l, rule)
	return true, nil
}

// ScanFile scans a file path with the provided Yara rules.
func (s *Scanner) ScanFile(filePath string) ([]*yara.Rule, error) {
	var matches matchList

	// Scan the file.
	err := s.rules.ScanFile(filePath, 0, 60*time.Second, &matches)

	// Return any results.
	return []*yara.Rule(matches), err
}
