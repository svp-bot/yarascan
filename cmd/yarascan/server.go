package main

import (
	"context"
	"flag"
	"io/ioutil"
	"log"
	"net/http"

	"git.autistici.org/ai3/go-common/serverutil"
	"git.autistici.org/ai3/tools/yarascan/server"
	"git.autistici.org/ai3/tools/yarascan/server/db"
	"git.autistici.org/ai3/tools/yarascan/server/ui"
	"github.com/google/subcommands"
	yaml "gopkg.in/yaml.v2"
)

// Type that wraps together all the server configuration pieces that
// aren't expressed by command-line flags, so that it can be read from
// a YAML file.
type serverConfig struct {
	server.Config `yaml:",inline"`
	HTTPConfig    *serverutil.ServerConfig `yaml:"http_server"`
}

type serverCommand struct {
	addr       string
	configPath string
	dbPath     string
	disableUI  bool
}

func (c *serverCommand) Name() string     { return "server" }
func (c *serverCommand) Synopsis() string { return "run the HTTP server" }
func (c *serverCommand) Usage() string {
	return `server [<flags>]:
	Run the HTTP server.

`
}

func (c *serverCommand) SetFlags(f *flag.FlagSet) {
	f.StringVar(&c.addr, "addr", ":3419", "tcp `address` to listen to")
	f.StringVar(&c.configPath, "config", "/etc/yarascan/server.yml", "configuration `file`")
	f.StringVar(&c.dbPath, "db", "/var/lib/yarascan/detections.db?_journal=WAL", "database `path`")
	f.BoolVar(&c.disableUI, "no-ui", false, "disable the UI")
}

// Build the server.Config object out of command-line flags.
func (c *serverCommand) loadConfig() (*serverConfig, error) {
	data, err := ioutil.ReadFile(c.configPath)
	if err != nil {
		return nil, err
	}
	var config serverConfig
	if err := yaml.Unmarshal(data, &config); err != nil {
		return nil, err
	}
	return &config, nil
}

func (c *serverCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() > 0 {
		log.Printf("error: too many arguments")
		return subcommands.ExitUsageError
	}

	database, err := db.Open(c.dbPath)
	if err != nil {
		log.Printf("error: database initialization failed: %v", err)
		return subcommands.ExitFailure
	}
	defer database.Close()

	config, err := c.loadConfig()
	if err != nil {
		log.Printf("error: could not read server configuration: %v", err)
		return subcommands.ExitFailure
	}

	srv, err := server.New(database, &config.Config)
	if err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}
	defer srv.Close()

	uiSrv := ui.New(database)

	// Build the final handler with ui + API.
	h := http.NewServeMux()
	h.Handle("/api/", srv.Handler())
	if !c.disableUI {
		h.Handle("/", uiSrv.Handler())
	}

	if err := serverutil.Serve(h, config.HTTPConfig, c.addr); err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}

	return subcommands.ExitSuccess
}

func init() {
	subcommands.Register(&serverCommand{}, "")
}
