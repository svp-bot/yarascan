package main

import (
	"bufio"
	"encoding/json"
	"io"
	"log"
	"sync"
)

type jsonRowReader struct {
	scanner *bufio.Scanner
}

func newJSONRowReader(r io.Reader) *jsonRowReader {
	return &jsonRowReader{scanner: bufio.NewScanner(r)}
}

func (r *jsonRowReader) Scan(obj interface{}) error {
	for r.scanner.Scan() {
		if err := json.Unmarshal(r.scanner.Bytes(), obj); err != nil {
			log.Printf("corrupted line: %v", err)
			continue
		}
		return nil
	}
	return io.EOF
}

type jsonRowWriter struct {
	w  *bufio.Writer
	mx sync.Mutex
}

func newJSONRowWriter(w io.Writer) *jsonRowWriter {
	return &jsonRowWriter{w: bufio.NewWriter(w)}
}

func (w *jsonRowWriter) Dump(obj interface{}) error {
	w.mx.Lock()
	defer w.mx.Unlock()

	data, err := json.Marshal(obj)
	if err != nil {
		return err
	}
	if _, err = w.w.Write(data); err != nil {
		return err
	}
	_, err = io.WriteString(w.w, "\n")
	return err
}

func (w *jsonRowWriter) Flush() {
	w.w.Flush()
}
