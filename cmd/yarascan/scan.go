package main

import (
	"bufio"
	"context"
	"flag"
	"log"
	"os"
	"runtime"
	"sync"

	"github.com/google/subcommands"

	"git.autistici.org/ai3/tools/yarascan"
)

type scanCommand struct {
	rulesPath  string
	numWorkers int
}

func (c *scanCommand) Name() string     { return "scan" }
func (c *scanCommand) Synopsis() string { return "scan filesystem" }
func (c *scanCommand) Usage() string {
	return `scan [<flags>]:
	Scan the filesystem. List of files to be scanned must be
	passed on standard input, one file per line.

`
}

func (c *scanCommand) SetFlags(f *flag.FlagSet) {
	f.StringVar(&c.rulesPath, "rules", "", "YARA rules `path`")
	f.IntVar(&c.numWorkers, "num-workers", runtime.NumCPU(), "number of concurrent rule workers")
}

func (c *scanCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() > 0 {
		log.Printf("error: too many arguments")
		return subcommands.ExitUsageError
	}

	s, err := yarascan.NewScanner(c.rulesPath)
	if err != nil {
		log.Fatal(err)
	}

	// Start the workers and feed them the list of paths from stdin.
	var wg sync.WaitGroup
	outCh := make(chan *yarascan.Detection, 100)
	inputCh := make(chan string, 100)
	for i := 0; i < c.numWorkers; i++ {
		wg.Add(1)
		go func() {
			c.scanWorker(s, inputCh, outCh)
			wg.Done()
		}()
	}
	go func() {
		wg.Wait()
		close(outCh)
	}()
	go func() {
		input := bufio.NewScanner(os.Stdin)
		for input.Scan() {
			inputCh <- input.Text()
		}
		close(inputCh)
	}()

	// Collect and output results.
	w := newJSONRowWriter(os.Stdout)
	defer w.Flush()
	for d := range outCh {
		if err := w.Dump(d); err != nil {
			log.Printf("error: %v", err)
			return subcommands.ExitFailure
		}
	}

	return subcommands.ExitSuccess
}

func (c *scanCommand) scanWorker(s *yarascan.Scanner, inputCh <-chan string, ch chan<- *yarascan.Detection) {
	for path := range inputCh {
		// Only scan regular files. Checks for existence as a side effect.
		fi, err := os.Stat(path)
		if err != nil {
			log.Printf("error: %s: %v", path, err)
			continue
		}
		if !fi.Mode().IsRegular() {
			continue
		}

		matches, err := s.ScanFile(path)
		if err != nil {
			log.Printf("%s: %v", path, err)
			continue
		}
		for _, rule := range matches {
			ch <- yarascan.NewDetection(path, rule.Identifier())
		}
	}
}

func init() {
	subcommands.Register(&scanCommand{}, "")
}
