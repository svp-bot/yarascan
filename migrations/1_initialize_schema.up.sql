
CREATE TABLE detections (
       id INTEGER PRIMARY KEY,
       site TEXT,
       host TEXT,
       path TEXT,
       signature TEXT,
       timestamp DATETIME,
       notified BOOL NOT NULL DEFAULT 0,
       resolved BOOL NOT NULL DEFAULT 0
);

CREATE INDEX idx_detections_site ON detections (site);
CREATE INDEX idx_detections_host_path ON detections (host, path);
CREATE INDEX idx_detections_signature ON detections (signature);
